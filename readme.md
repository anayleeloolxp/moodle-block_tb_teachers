# Leeloo LXP Teachers Block
this block allows you to show teachers blocks added in Leeloo LXP to. be shown on moodle.
Installation Instructions
=========================

* Make sure you have all the required versions.
* Download and unpack the block folder.
* Place the folder (eg "tb_teachers") in the "blocks" subdirectory.
* Visit http://yoursite.com/admin to complete the installation
* Turn editing on the my page.
* Add the block to the page ("Leeloo LXP Teachers Block (tb_teachers)")
* Visit the config link in the block for more options.

License
=====================

GPL 3 or later